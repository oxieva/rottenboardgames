<?php

namespace App\Admin;

use App\Entity\Category;

use App\Entity\Game;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

final class GameAdmin extends AbstractAdmin
{
    public function toString($object)
    {
        return $object instanceof Game
            ? $object->getName()
            : 'Game'; // shown in the breadcrumb on the create view
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('name', TextType::class)
                ->add('designer', TextType::class)
                ->add('number_players', IntegerType::class)
            ->end()
            ->with('Data', array('class' => 'col-md-6'))
                ->add('description', TextareaType::class)
                ->add('playing_time', TextType::class)
                ->add('rating', NumberType::class)
            ->end()

            ->with('Category')
                ->add('Category', ModelType::class, [
                        'class' => Category::class,
                        'property' => 'name',
                /*->add('category', 'sonata_type_model', [
                'admin_code' => 'admin.category',
                'property' => 'name',
                'multiple' => true*/
            ])
            ->end()
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('designer', null, array('label' => 'Designer/Author'))
            ->add('description')
            ->add('playing_time')
            ->add('rating',  null, [
                'label_icon' => 'fa fa-thumbs-o-up'
            ])
            ->add('category.name')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('designer', null, [
                'show_filter' => true
            ])
            ->add('description');
    }
}