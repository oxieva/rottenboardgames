<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 14/09/19
 * Time: 12:13
 */

namespace App\Controller;

use App\Entity\Game;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\GameRepository;

class ShowGamesController extends AbstractController
{

    /**
     * @Route("/games", name="app_showgames")
     */
    public function showGames()
    {
        $games =$this->getDoctrine()->getRepository(Game::class)->findAll();

        $response = $this->render('pages/showgames.html.twig',[
            'games' => $games,
        ]);

        // cache for 3600 seconds
        $response->setSharedMaxAge(3600);
        $response->setMaxAge(3600);
        // (optional) set a custom Cache-Control directive
        $response->headers->addCacheControlDirective('must-revalidate', true);
        return $response;
        //return $this->render('pages/showgames.html.twig');
    }

}